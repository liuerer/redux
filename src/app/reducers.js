import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import userProfileReducer from "./user/reducers/userProfileReducer";

export default combineReducers({
  product: productReducer,
  user: userProfileReducer
});
