export const getUserProfiles = (userId) => (dispatch) => {
  fetch(`http://localhost:8080/api/user-profiles/${userId}`)
	  .then(response=>response.json())
	  .then(responseJson => {
		  dispatch({
			  type: 'GET_USER_PROFILES',
			  userProfiles: responseJson
		  });
	  })
	  .catch(console.log);
};
