import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {getUserProfiles} from "../actions/userProfileAction";
import {connect} from "react-redux";
import Profiles from "../componets/Profiles";
import {Link} from "react-router-dom";

class UserProfile extends Component{

	componentDidMount() {
		this.props.getUserProfiles(this.props.match.params.id);
	}

	render() {
		const {name, gender, description} = this.props.userProfiles;

		return(
			<div>
				<h2>User Profile</h2>
				<Profiles name={name} gender={gender} description={description} />
				<Link to='/'>Back Home</Link>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	userProfiles: state.user.userProfiles
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getUserProfiles
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
