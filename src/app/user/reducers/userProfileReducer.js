const initState = {
	userProfiles: {}
};

export default (state = initState, action) => {
	switch (action.type) {
		case 'GET_USER_PROFILES':
			return {
				...state,
				userProfiles: action.userProfiles
			};

		default:
			return state;
	}
};
